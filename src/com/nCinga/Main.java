package com.nCinga;

import com.nCinga.bo.Buyer;
import com.nCinga.service.impl.BuyerServiceImpl;
import com.nCinga.service.impl.OrderServiceImpl;
import com.nCinga.service.impl.ProductServiceImpl;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        //---getting buyer's name
        Scanner inputScName = new Scanner(System.in);
        System.out.print("Buyer User name: ");
        String name = inputScName.nextLine();

        BuyerServiceImpl buyerService = new BuyerServiceImpl();

        orderManagement(buyerService.checkOrCreateBuyer(name));

    }

    public static void orderManagement(Buyer buyer) {
        //---------user instructions for selection
        System.out.println("=============++++++Order Management System++++++++=============== ");
        System.out.println("==================User(Buyer) Instructions============ ");
        System.out.println("Press 1 === View the products ");
        System.out.println("Press 2 === Order a product ");
        System.out.println("Press 3 === View my orders ");
        System.out.println("Press 4 === View orders for a particular seller ");
        System.out.println("Press 5 === View products for a particular seller ");
        System.out.println("Press Any Other to Exit");

        Scanner inputScChoice = new Scanner(System.in);
        System.out.print("Enter: ");
        int inputChoice = inputScChoice.nextInt();

        ProductServiceImpl productService = new ProductServiceImpl();
        OrderServiceImpl orderService = new OrderServiceImpl();
        switch (inputChoice) {
            case 1:
                productService.viewProducts();
                break;
            case 2:
                System.out.println("=============================== ");
                System.out.print("Enter the product ID : ");
                int inputProductId = inputScChoice.nextInt();
                System.out.println("=============================== ");
                System.out.print("Enter the product Quantity : ");
                int inputOrderQuantity = inputScChoice.nextInt();
                orderService.placeAnOrderService(inputProductId, buyer.getBuyerId(), inputOrderQuantity);
                break;
            case 3:
                orderService.viewBuyerOrders(buyer.getBuyerId());
                break;
            case 4:
                System.out.print("Enter Seller ID : ");
                int sellerIdCase4 = inputScChoice.nextInt();
                orderService.viewSellerOrders(sellerIdCase4);
                break;
            case 5:
                System.out.print("Enter Seller ID : ");
                int sellerIdCase5 = inputScChoice.nextInt();
                productService.viewSellerProducts(sellerIdCase5);
                break;
        }
    }
}
