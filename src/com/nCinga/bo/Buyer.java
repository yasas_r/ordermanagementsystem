package com.nCinga.bo;

import java.util.Objects;

public class Buyer {

    private int buyerId = 0;
    private static int autoIncId = 0;
    private final String buyerName;

    public Buyer(String buyerName) {
        this.buyerId = autoIncrementId();
        this.buyerName = buyerName;
    }

    public int getBuyerId() {
        return buyerId;
    }

    public String getBuyerName() {
        return buyerName;
    }

    public int autoIncrementId() {
        return ++autoIncId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Buyer)) return false;
        Buyer buyer = (Buyer) o;
        return getBuyerId() == buyer.getBuyerId();
    }

    @Override
    public String toString() {
        return "Buyer{" +
                "buyerId=" + buyerId +
                ", buyerName='" + buyerName + '\'' +
                '}';
    }
}
