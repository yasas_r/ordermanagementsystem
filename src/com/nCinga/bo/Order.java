package com.nCinga.bo;

import java.util.Objects;

public class Order {

    private int orderId = 0;
    private static int autoIncId = 0;
    private final Buyer buyer;
    private final Product product;
    private int orderedQuantity;
    private String OrderedDate;

    public Order(Buyer buyer, Product product, int orderedQuantity, String orderedDate) {
        this.orderId = autoIncrementId();
        this.buyer = buyer;
        this.product = product;
        this.orderedQuantity = orderedQuantity;
        OrderedDate = orderedDate;
    }

    public int getOrderId() {
        return this.orderId;
    }

    public Buyer getBuyer() {
        return this.buyer;
    }

    public Product getProduct() {
        return this.product;
    }

    public int getOrderedQuantity() {
        return this.orderedQuantity;
    }

    public String getOrderedDate() {
        return this.OrderedDate;
    }

    public int autoIncrementId() {
        return ++autoIncId;
    }

    public void setOrderedQuantity(int orderedQuantity) {
        this.orderedQuantity = orderedQuantity;
    }

    public void setOrderedDate(String orderedDate) {
        OrderedDate = orderedDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Order)) return false;
        Order order = (Order) o;
        return getOrderId() == order.getOrderId();
    }

    @Override
    public String toString() {
        return "Order{" +
                "orderId=" + orderId +
                ", buyer=" + buyer +
                ", product=" + product +
                ", orderedQuantity=" + orderedQuantity +
                ", OrderedDate='" + OrderedDate + '\'' +
                '}';
    }
}
