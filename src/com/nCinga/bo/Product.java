package com.nCinga.bo;

import java.util.Objects;

public class Product {

    private int productId = 0;
    private static int autoIncId = 0;
    private final Seller seller;
    private final String productName;
    private final int productPrice;
    private int availableQuantity;

    public Product(Seller seller, String productName, int productPrice, int availableQuantity) {
        this.productId = autoIncrementId();
        this.seller = seller;
        this.productName = productName;
        this.productPrice = productPrice;
        this.availableQuantity = availableQuantity;
    }

    public int autoIncrementId() {
        return ++autoIncId;
    }

    public int getProductId() {
        return this.productId;
    }

    public Seller getSeller() {
        return this.seller;
    }

    public String getProductName() {
        return this.productName;
    }

    public int getProductPrice() {
        return this.productPrice;
    }

    public int getAvailableQuantity() {
        return this.availableQuantity;
    }

    public void setAvailableQuantity(int availableQuantity) {
        this.availableQuantity = availableQuantity;
    }

    public void decreaseAvailableQuantity(){
        this.availableQuantity--;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Product)) return false;
        Product product = (Product) o;
        return getProductId() == product.getProductId() &&
                Objects.equals(getSeller(), product.getSeller());
    }

    @Override
    public String toString() {
        return "Product{" +
                ", seller=" + seller +
                ", productName='" + productName + '\'' +
                ", productPrice=" + productPrice +
                ", availableQuantity=" + availableQuantity +
                "}";
    }
}
