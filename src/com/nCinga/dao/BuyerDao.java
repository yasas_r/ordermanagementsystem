package com.nCinga.dao;

import com.nCinga.bo.Buyer;

import java.util.ArrayList;
import java.util.List;

public class BuyerDao {

    private static BuyerDao buyerDao;

    private List<Buyer> buyers;

    public BuyerDao() {
        this.buyers = new ArrayList<Buyer>();
    }

    public static BuyerDao getInstance() {
        buyerDao = buyerDao == null ? new BuyerDao() : buyerDao;
        return buyerDao;
    }

    public void addBuyer(Buyer buyer) {
        buyers.add(buyer);
    }

    public Buyer createAndAddBuyer(String buyerName) {
        Buyer buyer = new Buyer(buyerName);
        buyers.add(buyer);
        return buyer;
    }

    public void removeBuyer(Buyer buyer) {
        buyers.remove(buyer);
    }

    public Buyer getBuyerDetailsById(int buyerId) {
        for (Buyer buyer : buyers) {
            if (buyer.getBuyerId() == buyerId)
                return buyer;
        }
        return null;
    }

    public Buyer getBuyerByName(String buyerName) {
        for (Buyer buyer : buyers) {
            if (buyer.getBuyerName() == buyerName)
                return buyer;
        }
        return null;
    }

    public boolean isValidBuyer(int buyerId) {
        return getBuyerDetailsById(buyerId) != null ? true : false;
    }
}
