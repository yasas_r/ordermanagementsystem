package com.nCinga.dao;

import com.nCinga.bo.Order;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class OrderDao {

    private static OrderDao orderDao;

    private List<Order> orders;

    public OrderDao() {
        this.orders = new ArrayList<Order>();
    }

    public static OrderDao getInstance() {
        orderDao = orderDao == null ? new OrderDao() : orderDao;
        return orderDao;
    }

    public void addOrder(Order order) {
        orders.add(order);
    }

    public void removeOrder(Order order) {
        orders.remove(order);
    }

    public Order getOrderDetailsById(int orderId) {
        //order = (Order) orders.stream().filter(x -> (x.getOrderId() == orderId));
        for (Order order : orders) {
            if (order.getOrderId() == orderId)
                return order;
        }
        return null;
    }

    public List<Order> getOrdersForBuyer(int buyerId) {
        List<Order> resultOrders = orders.stream()
                .filter(order -> (order.getBuyer().getBuyerId() == buyerId)).collect(Collectors.toList());
        return resultOrders;
    }

    public List<Order> getOrdersForSeller(int sellerId) {
        List<Order> resultOrders = orders.stream()
                .filter(order -> (order.getProduct().getSeller().getSellerId() == sellerId)).collect(Collectors.toList());
        return resultOrders;
    }

    public boolean isExistOrder(int orderId) {
        return getOrderDetailsById(orderId) != null ? true : false;
    }

    public Order findOrderByProductAndBuyer(int productId, int buyerId) {
        for (Order order : orders) {
            if (order.getProduct().getProductId() == buyerId && order.getBuyer().getBuyerId() == buyerId)
                return order;
        }
        return null;
    }

    public void updateOrderedQuantity(int orderId, int orderedQuantity) {
        Order order = getOrderDetailsById(orderId);
        if (order != null) {
            order.setOrderedQuantity(orderedQuantity);
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = new Date();
            String currentData = dateFormat.format(date);
            order.setOrderedDate(currentData);
        }

    }


}
