package com.nCinga.dao;

import com.nCinga.bo.Product;
import com.nCinga.bo.Seller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ProductDao {

    private static ProductDao productDao;

    private List<Product> products;

    public ProductDao() {
        this.products = new ArrayList<Product>();
        initStoreItems();
    }

    public static ProductDao getInstance() {
        productDao = productDao == null ? new ProductDao() : productDao;
        return productDao;
    }

    private void initStoreItems() {
        products.add(new Product(new Seller(1, "seller1"), "product1", 400, 20));
        products.add(new Product(new Seller(1, "seller1"), "product2", 500, 15));
        products.add(new Product(new Seller(2, "seller2"), "product3", 300, 10));
    }

    public void addProduct(Product product) {
        products.add(product);
    }

    public void removeProduct(Product product) {
        products.remove(product);
    }


    public List<Product> getProducts() {
        List<Product> resultProducts = products.stream()
                .filter(product -> (product.getAvailableQuantity() > 0)).collect(Collectors.toList());
        return resultProducts;
    }

    public Product getProductById(int productId) {
        for (Product product : products) {
            if (product.getProductId() == productId)
                return product;
        }
        return null;
    }

    public List<Product> getProductsForSeller(int sellerId) {
        List<Product> resultProducts = products.stream()
                .filter(product -> (product.getSeller().getSellerId() == sellerId)).collect(Collectors.toList());
        return resultProducts;
    }

    public boolean isValidAndAvailableProduct(int productId) {
        return isValidProduct(productId) && getProductById(productId).getAvailableQuantity() > 0 ? true : false;
    }

    public boolean isValidProduct(int productId) {
        return getProductById(productId) != null ? true : false;
    }

    public void decreaseAvailableQuantity(int productId) {
        getProductById(productId).decreaseAvailableQuantity();
    }

}
