package com.nCinga.dao;

import com.nCinga.bo.Seller;

import java.util.ArrayList;
import java.util.List;

public class SellerDao {

    private static SellerDao sellerDao;

    private List<Seller> sellers;

    public SellerDao() {
        this.sellers = new ArrayList<Seller>();
    }

    public static SellerDao getInstance() {
        sellerDao = sellerDao == null ? new SellerDao() : sellerDao;
        return sellerDao;
    }

    public void addSeller(Seller seller) {
        sellers.add(seller);
    }

    public void removeSeller(Seller seller) {
        sellers.remove(seller);
    }

    public Seller getSellerDetailsById(int sellerId) {
        for (Seller seller : sellers) {
            if (seller.getSellerId() == sellerId)
                return seller;
        }
        return null;
    }

    public boolean isValidSeller(int sellerId) {
        return getSellerDetailsById(sellerId) != null ? true : false;
    }
}
