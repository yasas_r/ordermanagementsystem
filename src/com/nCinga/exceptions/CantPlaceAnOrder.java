package com.nCinga.exceptions;

public class CantPlaceAnOrder extends RuntimeException {

    public CantPlaceAnOrder(String message) {
        super(message);
    }
}
