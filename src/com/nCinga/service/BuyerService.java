package com.nCinga.service;


import com.nCinga.bo.Buyer;

public interface BuyerService {

    Buyer checkOrCreateBuyer(String buyerName);
}
