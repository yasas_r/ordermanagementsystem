package com.nCinga.service;

import com.nCinga.bo.Buyer;
import com.nCinga.bo.Product;

public interface OrderService {

    void placeAnOrderService(int productId, int buyerId, int orderedQuantity);

    void viewBuyerOrders(int buyerId);

    void viewSellerOrders(int sellerId);
}
