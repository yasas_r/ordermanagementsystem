package com.nCinga.service;

public interface ProductService {

    void viewProducts();

    void viewSellerProducts(int sellerId);
}
