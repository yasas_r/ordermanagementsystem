package com.nCinga.service.impl;

import com.nCinga.bo.Buyer;
import com.nCinga.dao.BuyerDao;
import com.nCinga.service.BuyerService;

public class BuyerServiceImpl implements BuyerService {

    private BuyerDao buyerDao;

    public BuyerServiceImpl() {
        buyerDao = BuyerDao.getInstance();
    }

    @Override
    public Buyer checkOrCreateBuyer(String buyerName) {
        Buyer buyer = buyerDao.getBuyerByName(buyerName);
        if (buyer != null)
            return buyer;
        else {
            buyer = buyerDao.createAndAddBuyer(buyerName);
            return buyer;
        }
    }

}
