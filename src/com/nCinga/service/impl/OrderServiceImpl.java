package com.nCinga.service.impl;

import com.nCinga.bo.Buyer;
import com.nCinga.bo.Order;
import com.nCinga.bo.Product;
import com.nCinga.dao.BuyerDao;
import com.nCinga.dao.OrderDao;
import com.nCinga.dao.ProductDao;
import com.nCinga.dao.SellerDao;
import com.nCinga.exceptions.CantPlaceAnOrder;
import com.nCinga.service.OrderService;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class OrderServiceImpl implements OrderService {

    private ProductDao productDao;
    private BuyerDao buyerDao;
    private OrderDao orderDao;
    private SellerDao sellerDao;

    public OrderServiceImpl() {
        productDao = ProductDao.getInstance();
        buyerDao = BuyerDao.getInstance();
        orderDao = OrderDao.getInstance();
        sellerDao = SellerDao.getInstance();
    }

    private boolean validateProduct(int productId) {
        return productDao.isValidAndAvailableProduct(productId);
    }

    private void placeAnOrder(int productId, int buyerId, int orderedQuantity) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        String currentData = dateFormat.format(date);
        Order order = orderDao.findOrderByProductAndBuyer(productId, buyerId);
        if (order == null) { // place new order
            orderDao.addOrder(new Order(buyerDao.getBuyerDetailsById(buyerId), productDao.getProductById(productId), orderedQuantity, currentData));
            productDao.decreaseAvailableQuantity(productId);
        } else { // Update the orderedQuantity if order is already exist
            orderDao.updateOrderedQuantity(order.getOrderId(), orderedQuantity);
        }
    }

    @Override
    public void placeAnOrderService(int productId, int buyerId, int orderedQuantity) {
        if (validateProduct(productId)) {
            placeAnOrder(productId, buyerId, orderedQuantity);
            System.out.println("Your Order is placed....");
        } else
            throw new CantPlaceAnOrder("Cant place the order");
    }

    @Override
    public void viewBuyerOrders(int buyerId) {
        List<Order> resultOrders = orderDao.getOrdersForSeller(buyerId);
        if (buyerDao.isValidBuyer(buyerId) && resultOrders.size() != 0)
            resultOrders.stream()
                    .forEach(order -> System.out.println(order.toString()));
        else
            System.out.println("There is no orders for you");
    }

    @Override
    public void viewSellerOrders(int sellerId) {
        List<Order> resultOrders = orderDao.getOrdersForSeller(sellerId);
        if (sellerDao.isValidSeller(sellerId))
            System.out.println("There is no such seller - Invalid seller ID");
        else if (resultOrders.size() != 0)
            resultOrders.stream().forEach(order -> System.out.println(order.toString()));
        else
            System.out.println("There is no orders from this seller");
    }
}
