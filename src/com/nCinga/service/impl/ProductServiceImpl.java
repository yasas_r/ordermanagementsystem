package com.nCinga.service.impl;

import com.nCinga.bo.Order;
import com.nCinga.bo.Product;
import com.nCinga.bo.Seller;
import com.nCinga.dao.ProductDao;
import com.nCinga.dao.SellerDao;
import com.nCinga.service.ProductService;

import java.util.List;

public class ProductServiceImpl implements ProductService {

    private ProductDao productDao;
    private SellerDao sellerDao;

    public ProductServiceImpl() {
        productDao = ProductDao.getInstance();
        sellerDao = SellerDao.getInstance();
    }

    @Override
    public void viewProducts() {
        List<Product> resultProducts = productDao.getProducts();
        if (resultProducts.size() != 0)
            resultProducts.stream().forEach(product -> System.out.println(product.toString()));
        else
            System.out.println("There is no available products now");
    }

    @Override
    public void viewSellerProducts(int sellerId) {
        List<Product> resultProducts = productDao.getProductsForSeller(sellerId);
        if (sellerDao.isValidSeller(sellerId))
            System.out.println("There is no such seller - Invalid seller ID");
        else if (resultProducts.size() != 0)
            resultProducts.stream().forEach(product -> System.out.println(product.toString()));
        else
            System.out.println("There is no products from this seller");
    }
}
